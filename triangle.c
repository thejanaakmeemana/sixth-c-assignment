#include <stdio.h>

int printLine(int);
int pattern(int);
int lineCount = 1;

//This function is to print a single line according to the intial value provided as argument
int printLine(int num){
  printf("%d",num);
  if(num>1) printLine(num-1);
  else printf("\n");
  return 0;
}

//This function is to control the number of lines that should be printed
int pattern(int n){
  printLine(lineCount);
  lineCount++;
  if (lineCount<=n) pattern(n);
  return 0;
}

int main(){
  int lineNumber;
  printf("Enter number of lines : ");
  scanf("%d",&lineNumber);
  pattern(lineNumber);
}
