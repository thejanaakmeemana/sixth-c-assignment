#include <stdio.h>

/*a = first value of every couple of numbers in fibonacci series
b = second value of every couple of numbers in fibonacci series
i = temporary variable used for switching numbers in fibonacci function
fibonacciCount = variable for incremeting index of fibonacci numbers printed*/

int a = 0,b = 1,i=0,fibonacciCount = 0;

int fibonacciSeq(int);

//This function is for printing fibonacci numbers
int fibonacciSeq(int n){
  printf("%d \n",a);
  fibonacciCount++;
  i = a;
  a = b;
  b = b+i;
  if(fibonacciCount<=n) fibonacciSeq(n);
  return 0;
}

int main(){
  int fibonacciNum = 0;
  printf("How many fibonacci numbers do you want except 0 ?? : ");
  scanf("%d",&fibonacciNum);
  fibonacciSeq(fibonacciNum);
}
